function snake(entityID, x, y, color)
{
	this.body = [];
	this.direction = "right";
	this.head = new cell(x, y);
	this.cells = null;
	this.color = color;
	this.isPlayer = null;

	// Server vars
	this.virtualKeyboard = null;
	this.entityID = entityID;

    this.init = function(isPlayer)
    {
    	console.log('Creating new player with entityID: ' + entityID);
    	console.log('x = ' + this.head.x + ' y = ' + this.head.y);
    	this.isPlayer = isPlayer;
		//make snake
		this.body.push(this.head);
		for (i = this.head.x-1; i > 0; i--) 
		{
            this.body.push(new cell(i, this.head.y));
        };

        if (!isPlayer)
        {
    		console.log(' and is an Entity');
        	this.virtualKeyboard = new bgVirtualKeyboard( this );

        	this.virtualKeyboard.keyPress = function(keyCode)
			{
				if (KEY.LEFT_ARROW == keyCode)
				{
					// console.log('Player 2 LEFT');
			    	keyBuffer[1].push("left");
				}
				if (KEY.UP_ARROW == keyCode )
				{
					// console.log('Player 2 UP');
			    	keyBuffer[1].push("up");
				}
				if (KEY.RIGHT_ARROW == keyCode )
				{
					// console.log('Player 2 RIGHT');
			    	keyBuffer[1].push("right");
				}
				if (KEY.DOWN_ARROW == keyCode )
				{
					// console.log('Player 2 DOWN');
			    	keyBuffer[1].push("down");
				}
			}
        }
    }

    this.update = function()
    {
        // Make the move
        switch(this.direction)
        {
            case "left":
            	this.cells = this.body.pop();
            	this.cells.x = (this.body[0].x - 1);
            	this.cells.y = (this.body[0].y);
            	this.body.unshift( this.cells );

		        if (this.collideWall())
		        {
		        	if (this.wallCollisionEnabled)
		        	{
		        		return true;
		        	}
		        	else
		        	{
		        		this.body[0].x = canvas.width/size - 1;
		        	}
		        }
            	break;
            case "right":
            	this.cells = this.body.pop();
            	this.cells.x = (this.body[0].x + 1);
            	this.cells.y = this.body[0].y;
            	this.body.unshift( this.cells );

		        if (this.collideWall())
		        {
		        	if (this.wallCollisionEnabled)
		        	{
		        		return true;
		        	}
		        	else
		        	{
		        		this.body[0].x = 0;
		        	}
		        }
	            break;
            case "up":
            	this.cells = this.body.pop();
            	this.cells.x = (this.body[0].x);
            	this.cells.y = (this.body[0].y - 1);
            	this.body.unshift( this.cells );

		        if (this.collideWall())
		        {
		        	if (this.wallCollisionEnabled)
		        	{
		        		return true;
		        	}
		        	else
		        	{
		        		this.body[0].y = canvas.height/size - 1;
		        	}
		        }
	            break;
            case "down":
            	this.cells = this.body.pop();
            	this.cells.x = (this.body[0].x);
            	this.cells.y = (this.body[0].y + 1);
            	this.body.unshift( this.cells );

		        if (this.collideWall())
		        {
		        	if (this.wallCollisionEnabled)
		        	{
		        		return true;
		        	}
		        	else
		        	{
		        		this.body[0].y = 0;
		        	}
		        }
	            break;
        };

        if ( this.isPlayer && this.collideSelf() )
        {
        	return true;
        }
    }

    this.draw = function()
    {
        context.strokeStyle = this.color;

        for ( this.cells in this.body)
        {
            this.cells = this.body[this.cells];
            context.strokeRect(this.cells.x * size,
                                this.cells.y * size, size, size);
        };
    }

    this.collideSelf = function()
    {
		// If you hit yourself, aka self collision, you die
		for (i = 1; i < this.body.length; i++)
		{
			this.cells = this.body[i];

			if ((this.body[0].x == this.cells.x &&
					this.body[0].y == this.cells.y) )
			{
				console.log('collideSelf');
				for (var j = 0; j < this.body.length; j++)
				{
					console.log('body part: ' + j + ' x: ' + this.body[j].x +
									' y: ' + this.body[j].y);
				}
				return true;
			};
		};

		return false;
    }

    this.collideWall = function()
    {
    	// If you go out of bounds you die
		if( this.body[0].x < 0 || this.body[0].x >= canvas.width/size || 
			this.body[0].y < 0 || this.body[0].y >= canvas.height/size )
		{
			// console.log('collideWall');
			return true;
		};

		return false;
    }

    this.addCell = function()
    {
    	//add cell to snake
    	this.body.push(new cell(this.body[this.body.length-1].x,
						  this.body[this.body.length-1].y));
    }

}

snake.prototype.wallCollisionEnabled = false;


//snake cells
var cell = function (x, y)
{
    this.x = x;
    this.y = y;
};


