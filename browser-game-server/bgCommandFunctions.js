var DefaultCmds = 
{
	// 0 - 99
	// Basic socket actions 
	messageToClent 	: function()
	{
		bgSocket.sendMsg(arguments[0]);
	},

	rcvCloseClient : function()
	{
		//Closing handshake
		if (bgSocket.WebSocket.readyState != socketState.CLOSING &&
			bgSocket.WebSocket.readyState != socketState.CLOSED )
		{
			console.log('[BGClient] Closing...');
			bgSocket.WebSocket.readyState = socketState.CLOSING;
			bgSocket.WebSocket.close(1000, "Closing correctly through Command");
		}
	},
	sndCloseClient : function()
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndCloseClient );
	},

	sndPong : function()
	{

	},

	rcvPing : function()
	{

	},


	// 100 - 999
	// JavaScript Event Support for entities
	// args entityId, keycode
	rcvEntityKeypress : function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('keyPress', cmd[2]);
	},

	rcvEntityKeyup	: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('keyUp', cmd[2]);
	},

	rcvEntityKeydown: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('keyDown', cmd[2]);
	},

	rcvEntityDragstart: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('dragStart', cmd[2]);
	},

	rcvEntityDragover: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('dragOver', cmd[2]);
	},

	rcvEntityFocus	: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('focus', cmd[2]);
	},

	rcvEntityBlur	: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('blur', cmd[2]);
	},

	rcvEntityClick	: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('click', cmd[2]);
	},

	rcvEntityDblClick: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('dblClick', cmd[2]);
	},

	rcvEntityMouseover: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('mouseOver', cmd[2]);
	},

	rcvEntityMouseout: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('mouseOver', cmd[2]);
	},

	rcvEntityMousewheel: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('mouseWheel', cmd[2]);
	},


	// JavaScript Event Support for player
	// arg keyCode
	sndPlayerKeypress: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerKeypress, entityId, keyCode);
	},

	sndPlayerKeyup	: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerKeyup, entityId, keyCode);
	},

	sndPlayerKeydown: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerKeydown, entityId, keyCode);
	},

	sndPlayerDragstart: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerDragstart, entityId, keyCode);
	},

	sndPlayerDragover: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerDragover, entityId, keyCode);
	},

	sndPlayerFocus	: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerFocus, entityId, keyCode);
	},

	sndPlayerBlur	: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerBlur, entityId, keyCode);
	},

	sndPlayerClick	: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerClick, entityId, keyCode);
	},

	sndPlayerDblClick: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerDblClick, entityId, keyCode);
	},

	sndPlayerMouseover: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerMouseover, entityId, keyCode);
	},

	sndPlayerMouseout: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerMouseout, entityId, keyCode);
	},

	sndPlayerMousewheel: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerMousewheel, entityId, keyCode);
	},

	// General Entity Updates
	rcvEntityPos	: function(cmd)// [Cmd, EntityID, X, Y]
	{
		var x = new Number(cmd[2]);
		x.toFixed(0);
		var y = new Number(cmd[3]);
		y.toFixed(0);

		bgClient.entityList[cmd[1]].body[0].x = x;
		bgClient.entityList[cmd[1]].body[0].y = y;
	},

	rcvRemoveEntity	: function(cmd)
	{
		
	},

	rcvCreateEntity	: function( cmd )// [Cmd, EntityID, X, Y]
	{
		var x = new Number(cmd[2]);
		x.toFixed(0);
		var y = new Number(cmd[3]);
		y.toFixed(0);
		
		snakes.push(new snake(cmd[1], x, y, 'red'));
		snakes[snakes.length-1].init(false);
		bgClient.entityList[cmd[1]] = snakes[snakes.length-1];
	},

	// 500 - 599
	// Player Functions
	sndJoinWorld	: function()
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndJoinWorld );
	},

	sndPlayerPos	: function(entityID, x, y)  // [entityID, x, y]
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerPos, entityID, x, y );
	},

	rcvPlayerPos	: function(cmd) // [Cmd, EntityID, X, Y]
	{

	},

	rcvInitPlayer	: function(cmd) // [Cmd, EntityID, X, Y]
	{
		var x = new Number(cmd[2]);
		x.toFixed(0);
		var y = new Number(cmd[3]);
		y.toFixed(0);

		snakes.push(new snake(cmd[1], x, y, 'white'));
		snakes[snakes.length-1].init(true);
		bgClient.entityList[cmd[1]] = snakes[snakes.length-1];
	},

	// 2000 - 32768
	// Developer cmds
	// <-------------------------- My Commands -------------------------------->
	rcvStartGame	: function()
	{
		start();
	},

	sndFoodEaten	: function() // entityID 
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndFoodEaten,
							arguments[0]);
	},

	rcvNewFood		: function(cmd) //cmdID x y
	{
		if (food === null)
		{
			console.log('MADE NEW FOOD');
			food = new Food(cmd[1], cmd[2]);
		}
		else
		{
			console.log('MADE FOOD');
			food.changePos(cmd[1], cmd[2]);
		}
	},

	sndReady		: function()
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndReady );
	},

	rcvGameOver		: function(cmd) // cmd, entityID
	{
		GameOver(cmd[1]);
	},

	sndGameOver		: function(entityID) // entityID
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndGameOver, entityID );
	},

	rcvAddEntityCell: function(cmd) //cmd, entityID
	{
		console.log('EntityID ' + cmd[1] + ' gets a cell');
		bgClient.entityList[cmd[1]].addCell();
	}

	
};


var DefaultCmdCodes =
{
	// 0 - 99
	// Basic socket actions 
	messageToClent 			: 0,
	rcvCloseClient			: 1,
	sndCloseClient			: 2,
	sndPong					: 3,
	rcvPing					: 4,

	// 100 - 999
	// JavaScript Event Support for entities
	rcvEntityKeypress 		: 100,
	rcvEntityKeyup			: 101,
	rcvEntityKeydown		: 102,
	rcvEntityDragstart		: 103,
	rcvEntityDragover		: 104,
	rcvEntityFocus			: 105,
	rcvEntityBlur			: 106,
	rcvEntityClick			: 107,
	rcvEntityDblClick		: 108,
	rcvEntityMouseover		: 109,
	rcvEntityMouseout		: 110,
	rcvEntityMousewheel 	: 111,

	sndPlayerKeypress		: 150, 
	sndPlayerKeyup			: 151, 
	sndPlayerKeydown		: 152, 
	sndPlayerDragstart		: 153, 
	sndPlayerDragover		: 154,
	sndPlayerFocus			: 155,
	sndPlayerBlur			: 156, 
	sndPlayerClick			: 157, 
	sndPlayerDblClick		: 158, 
	sndPlayerMouseover		: 159, 
	sndPlayerMouseout		: 160,
	sndPlayerMousewheel		: 161,

	// General Entity Updates
	rcvEntityPos			: 200,
	rcvRemoveEntity			: 201,
	rcvCreateEntity			: 202,
	// rcvEntityHP				: 203,
	// sndEntityStats			: 204,
	
	// 500 - 599
	// Player Functions
	sndJoinWorld			: 500,
	// rcvLoadWorld			: 501,
	// sndleaveWorld			: 502,
	// sndJoinLevel			: 503,
	// rcvLoadLevel			: 504,
	// sndPlayerHP				: 505,
	// rcvPlayerHP				: 506,
	sndPlayerPos			: 507,
	rcvPlayerPos			: 508,
	// sndPlayerStats			: 509,
	rcvInitPlayer			: 510,
	// sndchangeLevel			: 511

	// 2000 - 32768
	// Developer cmds
	rcvStartGame			: 2000,
	sndFoodEaten			: 2001, 
	rcvNewFood				: 2002,
	sndReady				: 2003,	
	rcvGameOver				: 2004,
	sndGameOver				: 2005,
	rcvAddEntityCell		: 2006

	// pos update sync
	// food eat sync
	// game over
	// game reset	
	// NTP research
	//local and online multiplayer



};







