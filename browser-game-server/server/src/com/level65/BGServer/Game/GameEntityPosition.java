/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.level65.BGServer.Game;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class GameEntityPosition 
{
   public double x = 0;
   public double y = 0;
   
   /**
    * Provides the ability to instantiate this object with a given x and y
    * coordinate.
    * @param _x
    * @param _y 
    */
   public GameEntityPosition( double _x, double _y )
   {
       this.x = _x;
       this.y = _y;
       
   }

   /**
    * Default constructor
    */
   public GameEntityPosition()
   {
   }

}
