/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.level65.BGServer.Game;

import com.level65.BGServer.Client.ClientMsg;
import java.util.ArrayList;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public interface GameLevel 
{
    
    /**
     * Returns the list of entities currently in the level.
     * @return 
     */
    abstract ArrayList<GameEntity> getEntities();
    
    /**
     * Returns a list of clients in the level by entityId
     * @return 
     */
    abstract ArrayList<Integer> getClients();
    
    //abstract GameEntity entity( int entityId );
    abstract int createEntity( GameEntity entity );
    abstract int createPlayer( int clientId, GameEntity player );
    abstract void removeEntity( int entityId );
    abstract void removePlayer( int clientId );
    abstract GameEntity getEntity( int entityId );
   
}
