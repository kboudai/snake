/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.level65.BGServer.Game;

import com.level65.BGServer.Client.ClientList;
import com.level65.BGServer.Client.ClientMsg;
import com.level65.BGServer.Client.ClientSender;
import com.level65.BGServer.Commands.CommandCodes;
import java.util.ArrayList;


/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class GameHandler 
{
    
    private static String clockType = null;
    private static ArrayList<GameLevel> levelList = new ArrayList<GameLevel>();
    
    
    private GameHandler() { }
    
    public static void init()
    {
        // Add test level.
        GameLevel TestLevel = new GameLevelTest();
        levelList.add( TestLevel );
        
        
    }
    

    public static String getClockType()
    {
        return clockType;
        
    }
    
    

    public static int joinLevel( int clientId, int levelId )
    {
        GameEntity player = new GameEntityPlayer();
        
        // TODO: Case for -1 returned (error).
        int entityId = registerLevelPlayer( clientId, levelId, player );
        ClientList.setLevelId( clientId, levelId );
        ClientList.setEntityId( clientId, entityId );
        return entityId;
        
    }
    
    public static int joinLevel( int clientId, int levelId, double x, double y )
    {
        GameEntity player = new GameEntityPlayer(x, y);
        
        // TODO: Case for -1 returned (error).
        int entityId = registerLevelPlayer( clientId, levelId, player );
        ClientList.setLevelId( clientId, levelId );
        ClientList.setEntityId( clientId, entityId );
        return entityId;
        
    }
    
    
    
    public static void sendMyChunkEntities( int clientId )
    {
        int chunkId = ClientList.getChunkId(clientId);
        int levelId = ClientList.getLevelId(clientId);
        // TODO: Chunk entityList
        
        
    }
    
    
    
    public static void sendLevelEntities( int clientId, int levelId )
    {
        
        int myEntityId = ClientList.getEntityId( clientId );
        if ( levelList.get( levelId ) != null )
        {
            int entityId = 0;
            for ( GameEntity entity : levelList.get( levelId ).getEntities() )
            {      
                if ( entity != null && entityId != myEntityId )
                {
                	GameEntityPosition pos = entity.getPosition();
                    ClientSender.send( clientId, 
                            new ClientMsg( CommandCodes.ENTITY_SND_CREATE, 
                            				entityId, pos.x, pos.y ) );
                    
                }
                
                ++entityId;
                
            }
            
        }
        
    }
    
    
    
    public static int registerLevelPlayer( int clientId, int levelId, GameEntity player )
    {
        if ( levelList.get( levelId ) != null )
        {
            int entityId = levelList.get( levelId ).createPlayer( 
                    clientId, player );
            
            ClientSender.notifyMyLevel( levelId, clientId, 
                new ClientMsg( CommandCodes.ENTITY_SND_CREATE, 
                    entityId, 
                    player.getPosition().x,
                    player.getPosition().y 
                    ) );          
            
            ClientList.setEntityId( clientId, entityId );
            
            return entityId;
            
        }
        else
        {
            return -1;
            
        }  
    }
    
    
    
    /**
     * Removes the player's entity from a level and notifies nearby players
     * to delete the entity.
     * @param clientId
     * @param levelId 
     */
    public static void unregisterLevelPlayer( int clientId, int levelId )
    {
        int entityId = ClientList.getEntityId( clientId );
               
        //this.notifyLevelEntityRemoved( sourceClientId, entityId );
        //ClientSender.notifyMyLevel( )
        
    }    
    
    
    /**
     * Creates the entity and notifies players in the level that it exists.
     * @param levelId
     * @param entity
     * @return 
     */
    public int registerEntity( int levelId, GameEntity entity )
    {
        // TODO: Checking for valid level
        
        if ( levelList.get( levelId ) != null )
        {
            
            return levelList.get( levelId ).createEntity( entity );
            
        }
        else
        {
            return -1;
            
        }
    }
    
    
    public static void leaveLevel( int clientId )
    {
        int levelId = ClientList.getLevelId( clientId );
        unregisterLevelPlayer( clientId, levelId );
        
    }
    
    
    public static void joinWorld( int clientId )
    {
      
        
    }
    

    
    
    /**
     * Registers a player player's entity within a specific chunk and notifies
     * players within that chunk to create the new player's entity.
     * @param entityId 
     */
    public static void registerChunkPlayer( int chunkId, int clientId )
    {
        
    }
    
    /**
     * Registers a player with a specific chunk and notifies players within
     * the chunk that the player's entity should be removed.
     * @param chunkId
     * @param clientId 
     */
    public static void unregisterChunkPlayer( int chunkId, int clientId )
    {
        
    }
    
    
    /**
     * Updates the position for a specific client.
     * @param clientId
     * @param x
     * @param y 
     */
    public static void updatePlayerPos( int clientId, int entityId, double x, double y )
    {
        int levelId = ClientList.getLevelId( clientId );
        updateEntityPos( levelId, entityId, x, y );
        ClientSender.notifyMyLevel( levelId, clientId,
                new ClientMsg( CommandCodes.ENTITY_SND_POS, entityId, x, y ) );
        
        
    }
    
    
    /**
     * Update the position for an entity.
     * @param levelId
     * @param entityId
     * @param x
     * @param y 
     */
    public static void updateEntityPos( int levelId, int entityId, double x,
            double y)
    {
        
        synchronized ( levelList.get( levelId ).getEntities() )
        {
            GameEntity entity = levelList.get( levelId ).getEntity( entityId );
            entity.setX( x );
            entity.setY( y );
        }
            
        
    }
    
    
    /**
     * Returns a pointer to the GameLevel object which can be used to access
     * different level properties.
     * @param levelId
     * @return 
     */
    public static GameLevel getLevel( int levelId )
    {
        if ( levelList.get( 0 ) != null )
        {
            return levelList.get( 0 );
            
        }
        else
        {
            return null;
            
        }
    }
    
    
    /**
     * Sets the value of clockType only if the provided type is valid.  We use
     * a mutator here to guarantee that clockType will have a valid value.
     * @param type 
     */
    public static void setClockType( String type )
    {
        if ( type == "turns" && type == "ticks" )
        {
            clockType = type;
            
        }
        else
        {
            // TODO: Show an error.
            
        }
    }
}
