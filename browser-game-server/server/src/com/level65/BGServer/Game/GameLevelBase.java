/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.level65.BGServer.Game;

import com.level65.BGServer.Client.ClientList;
import com.level65.BGServer.Client.ClientMsg;
import com.level65.BGServer.Client.ClientSender;
import com.level65.BGServer.Commands.CommandCodes;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class GameLevelBase implements GameLevel
{
    
    public ArrayList<GameEntity> entityList = new ArrayList<GameEntity>();
    public ArrayList<Integer> playerList = new ArrayList<Integer>();
    
    
    public ArrayList<GameEntity> getEntities()
    {
        return this.entityList;
        
    }
    
    public ArrayList<Integer> getClients()
    {
        return this.playerList;
        
    }
    
    
    
    public void broadcastEntityPos( int entityId )
    {
        /* this.broadcastEntityPos( entityId, 
                this.entityList.get( entityId ).getPosition().x,
                this.entityList.get( entityId ).getPosition().y
                );
                **/
        
    }
    
    
    public ArrayList<Integer> getPlayerList()
    {
        return playerList;
        
    }   
    
    
    
    public GameEntity getEntity( int entityId )
    {
        // TODO: Proper safety handling for non-existant records.
        return this.entityList.get( entityId );
        
    }
    
    
    
    /**
     * Creates the player's entity and adds them to the level's 
     * entityId->clientId list. This is done in a thread-safe way.
     * @param clientId
     * @param entity
     * @return 
     */
    public int createPlayer( int clientId, GameEntity entity )
    {
        int entityId = this.createEntity( entity );
        
        synchronized ( this.playerList )
        {
            // This list gives a quick way to find the entityId for all
            // clients in the area.
            this.playerList.add( entityId, clientId );
            
        }
        
        return entityId;
        
    }
    
    
    /**
     * Adds an entity object to the level's entity list.
     * @param entity
     * @return 
     */
    public int createEntity( GameEntity entity )
    {
        synchronized ( this.entityList )
        {
            this.entityList.add( entity );
            return this.entityList.size() - 1;
            
        }
    }
    
    
    public void sendNewEntities( int clientId )
    {
    	// TODO: this and sendFromPlayer are assigned
    	//			to the same command code?!?!
    	System.out.println("sendNewEntities create this entity with this pos");
        
        int myEntityId = ClientList.getEntityId( clientId );
        for ( GameEntity entity : this.entityList )
        {
            
            int entityId = this.entityList.indexOf( entity );
            if ( entityId != myEntityId )
            {
                
                ClientSender.send( clientId, new ClientMsg(
                        CommandCodes.ENTITY_SND_CREATE,
                        entity.getPosition().x,
                        entity.getPosition().y 
                        ) );
                
            }
        }

    }
    
    
    
    public void removePlayer( int clientId )
    {
        synchronized ( this.playerList )
        {
            int entityId = ClientList.getEntityId( clientId );
            this.playerList.remove( entityId );
            
        }
    }
   
    
    
    public void removeEntity( int entityId )
    {
        // TODO: Proper safety handling for non-existant records.
        this.entityList.remove( entityId );
        
    }
}
