/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.level65.BGServer.Game;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class GameEntityBase implements GameEntity
{
    double hp = 0;
    GameEntityPosition pos;
    int clientId = -1;
    
    public GameEntityBase()
    {
        this.pos = new GameEntityPosition( 0, 0 );
        
    }
    
    public GameEntityBase(double x, double y)
    {
        this.pos = new GameEntityPosition( x, y );
        
    }
    
    public GameEntityPosition getPosition()
    {
        return this.pos;
        
    }
    
    public String getState()
    {
        return "";
    
    }
    
    
    public int getClientId()
    {
        return this.clientId;
        
    }
    
    
    
    public String getSprite()
    {
        return "";
        
    }
    
    public String getStats()
    {
        return "";
        
    }
    
    
    public void setX( double x )
    {
        synchronized ( this.pos )
        {
            this.pos.x = x;
        
        }
    }
    
    
    public void setY( double y )
    {
        synchronized ( this.pos )
        {
            this.pos.y = y;
        
        }
    }
    
    
    
    public double getHP()
    {
        return this.hp;
        
    }
}
