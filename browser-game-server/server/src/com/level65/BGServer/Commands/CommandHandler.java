/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.BGServer.Commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class CommandHandler 
{
    
    public static HashMap<Short,Command> commandMap;
    
    // Private to prevent instantiation. This class is designed to function
    // efficient statically.
    private CommandHandler() { }
    
    
    /**go 
     * Initializes the CommandHandler. MUST be called before any methods
     * are used.
     * 
     */
    public static void init()
    {
        System.out.println( "[BGServer] CommandHandler init" );
        commandMap = new HashMap<Short,Command>();
        
        new CommandListClient();      
        new CommandListGame();
        new CommandListKeyboardEvt();
        
    } // ef
    
    /**
     * Takes a commandList object and adds the list of commands to commandMap.
     * Duplicate keys will result in an exception thrown.
     * @param commandList   The HashMap<String,Command> object that should be
     * added to the current commandMap.
     */
    public static void registerCommandList( HashMap<Short,Command> commandList )
    {
        System.out.println( "[BGServer] Adding commands..." );
        Iterator iter = commandList.keySet().iterator();
        
        while ( iter.hasNext() )
        {
            try 
            {
                Short key = (Short) iter.next();

                if ( commandMap.containsKey (key) ) 
                {
                    // TODO: Give this a real exception msg.
                    throw new CommandExistsException( "Something" );

                }            
                commandMap.put( key, (Command)commandList.get( key ) );
                
            }
            catch ( CommandExistsException e )
            {
                e.printStackTrace();
            
            }
        }
    } // ef
    
    public static void registerCommands( Short commandCode, Command command )
    {
//        System.out.println( "[BGServer] Adding command..." );
        try
        {
	        if ( commandMap.containsKey (commandCode) ) 
	        {
	            // TODO: Give this a real exception msg.
	            throw new CommandExistsException( "Something" );
	
	        }            
	        
	        commandMap.put( commandCode, command );
                
        }
        catch ( CommandExistsException e )
        {
            e.printStackTrace();
        
        }
    } // ef
    
    
    /**
     * Checks the commandMap to see whether this command has been loaded into it
     * already.
     * @param commandName
     * @return
     */
    public static boolean hasCommand( short commandName )
    {
        if ( commandMap.containsKey( commandName ) )
        {
            return true;
    
        }
        else
        {
            return false;
            
        }
    } // ef
    
    
    /**
     * Calls the specified command and passes the list of args to the specified
     * method.
     * @param clientId
     * @param commandName
     * @param args
     * @return 
     */
    public static boolean doCommand( int clientId, short commandName, 
            ArrayList<String> args )
    {
        System.out.printf( "[BGServer] Try Command: %s%n", commandName );
        if ( hasCommand( commandName ) )
        {
            if ( ( commandMap.get( commandName ) ).getArgCount() >= args.size() - 1 )
            {    
                ( commandMap.get( commandName ) ).callCommand( clientId, args );
                return true;
            
            }
            else
            {
                System.err.printf( "[BGServer] Invalid argcount for command: %s%n.", commandName );
                return false;
                
            }
        }
        else
        {
            System.err.println( "[BGServer] No such command\n" );
            return false;
            
        }
    } // ef

}
