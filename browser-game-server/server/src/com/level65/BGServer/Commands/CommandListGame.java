/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.level65.BGServer.Commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import com.level65.BGServer.Client.ClientMsg;
import com.level65.BGServer.Client.ClientOutputHandler;
import com.level65.BGServer.Client.ClientSender;
import com.level65.BGServer.Game.GameHandler;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class CommandListGame
{
    public CommandListGame()
    {
    	// Player Join world
        // Join World. No args
        CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_JOIN_WORLD, new Command()
        {
           public void callCommand( int clientId, ArrayList<String> args )
           {
        	   System.out.println( "Creating player..." );
               
               double size = GameHandler.getLevel(0).getEntities().size();
               if ( size < 2  )
               {
            	   double x = 5;
            	   double y = size;
                   int entityId = GameHandler.joinLevel( clientId, 0, x, y );
                   ClientSender.send( clientId, 
                      new ClientMsg( CommandCodes.PLAYER_SND_INIT, entityId, x, y ) );
                   GameHandler.sendLevelEntities( clientId, 0 );
                   //GameHandler.getLevel( 0 ).sendNewEntities( clientId );
               }
               else
               {
            	   System.out.println("To many players!!");
               }
               
           }
           
           public int getArgCount() { return 0; }
            
        });   

        
//        CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_LEAVE_WORLD, new Command()
//        {
//            public void callCommand( int clientId, ArrayList<String> args )
//            {
//            }
//            
//            public int getArgCount() { return 0; }
//            
//        } );
//
//        CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_JOIN_LEVEL, new Command()
//        {
//            public void callCommand( int clientId, ArrayList<String> args )
//            {
//                // Reply with a PLAYER_SND_LOAD_LEVEL or not.
//                
//            }
//            
//            public int getArgCount() { return 0; }
//            
//        } ); 
//
//
//        CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_HP, new Command()
//        {
//            public void callCommand( int clientId, ArrayList<String> args )
//            {
//            }
//            
//            public int getArgCount() { return 0; }
//            
//        } ); 

        
        
        // UPDATE PLAYER POSITION
        // X, Y
        CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_POS, new Command()
        {
           public void callCommand( int clientId, ArrayList<String> args )
           {
               try
               {
            	   int entityId = Integer.parseInt( args.get( 0 ) );
                   double x = Double.parseDouble( args.get( 1 ) );
                   double y = Double.parseDouble( args.get( 2 ) );

                   GameHandler.updatePlayerPos( clientId, entityId, x, y );
                   
               }
               catch ( NumberFormatException e )
               {
                   e.printStackTrace();
               }
               
           }
           
           public int getArgCount() { return 2; }
            
        }); 
 

//        CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_STATS, new Command()
//        {
//            public void callCommand( int clientId, ArrayList<String> args )
//            {
//            }
//            
//            public int getArgCount() { return 0; }
//            
//        } );
//
//
//        CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_CHANGE_LEVEL, new Command()
//        {
//            public void callCommand( int clientId, ArrayList<String> args )
//            {
//            }
//            
//            public int getArgCount() { return 0; }
//            
//        } );
        
        CommandHandler.registerCommands( CommandCodes.LEVEL_RCV_FOODEATEN, new Command()
        {
     	   public Random foodPosGen = new Random();
     	   
           public void callCommand( int clientId, ArrayList<String> args )
           {
        	   int x = foodPosGen.nextInt(50);
        	   int y = foodPosGen.nextInt(50);
               
        	   ClientSender.notifyAll(
        			   new ClientMsg(CommandCodes.LEVEL_SND_NEWFOOD, x, y));
        	   
        	   CommandHandler.commandMap.get(
        			   CommandCodes.ENTITY_SND_ADDCELL).callCommand
        			   (clientId, args);
               
           }
           
           public int getArgCount() { return 0; }
            
        });

        CommandHandler.registerCommands( CommandCodes.LEVEL_RCV_READY, new Command()
        {

     	   public int playerCount = 0;
     	   public Random foodPosGen = new Random();
     	   
           public void callCommand( int clientId, ArrayList<String> args )
           {
        	   ++this.playerCount;

        	   if(playerCount == 2)
        	   {
        		   System.out.println("Game Start");
        		   
            	   int x = foodPosGen.nextInt(50);
            	   int y = foodPosGen.nextInt(50);
//                	   synchronized ( ClientOutputHandler.notifyAllBuffer )
//                       {
//	                	   ClientOutputHandler.notifyAllBuffer.add(new ClientMsg(CommandCodes.LEVEL_SND_NEWFOOD, x, y).msg);
//	                	   ClientOutputHandler.notifyAllBuffer.add(new ClientMsg(CommandCodes.LEVEL_SND_START).msg);
//	                	   ClientOutputHandler.notifyAllBuffer.notifyAll();
//                       }
        		   ClientSender.notifyAll(
       				   	new ClientMsg(CommandCodes.LEVEL_SND_NEWFOOD, x, y));
//            		   ++ClientOutputHandler.nABCount;
        		   ClientSender.notifyAll(
       				   	new ClientMsg(CommandCodes.LEVEL_SND_START));
        	   }

               
           }
           
           public int getArgCount() { return 0; }
            
        }); 

        CommandHandler.registerCommands( CommandCodes.ENTITY_SND_ADDCELL, new Command()
        {
        	public void callCommand( int clientId, ArrayList<String> args )
           {   
               try
               {
             	   int entityId = Integer.parseInt( args.get( 0 ) );
             	   
             	   System.out.println("add food to entityID " + entityId);
             	   
            	   ClientSender.notifyAll(
            	   new ClientMsg(CommandCodes.ENTITY_SND_ADDCELL, entityId));
                   
               }
               catch ( NumberFormatException e )
               {
                   e.printStackTrace();
               }
               
           }
           
           public int getArgCount() { return 0; }
            
        });

        CommandHandler.registerCommands( CommandCodes.LEVEL_RCV_GAMEOVER, new Command()
        {
        	public void callCommand( int clientId, ArrayList<String> args )
           {   
               try
               {
            	   System.out.println( "GAME OVER" );
             	   int entityId = Integer.parseInt( args.get( 0 ) );
             	   
            	   ClientSender.notifyAll(
            	   new ClientMsg(CommandCodes.LEVEL_SND_GAMEOVER, entityId));
                   
               }
               catch ( NumberFormatException e )
               {
                   e.printStackTrace();
               }
               
           }
           
           public int getArgCount() { return 0; }
            
        });
        
    }
}
