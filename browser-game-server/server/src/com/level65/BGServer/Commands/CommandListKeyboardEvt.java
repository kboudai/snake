/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.level65.BGServer.Commands;

import java.util.ArrayList;

import com.level65.BGServer.Client.ClientMsg;
import com.level65.BGServer.Client.ClientSender;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class CommandListKeyboardEvt
{
	public CommandListKeyboardEvt()
    {
        // <-------------------------------RCV-------------------------------->

		// Javascript Keypress Event
		CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_KEYPRESS, new Command()
		{
		    public void callCommand( int clientId, ArrayList<String> args )
		    {
		        String keycode = args.get(0).toString();
		        String entityId = args.get(1).toString();
		        		        
		        ClientSender.notifyMyLevel( clientId, 
		                new ClientMsg( CommandCodes.ENTITY_SND_KEYPRESS,
		                keycode,
		                entityId ) );
		        
		        
		    }
		    
		    public int getArgCount() { return 2; }
		    
		} );
		
		// Javascript Keyup Event
		CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_KEYUP, new Command()
		{
		    /**
		     * args[0] = keycode
		     * args[1] = entityId
		     */
		    public void callCommand( int clientId, ArrayList<String> args )
		    {
		        String keycode = args.get(0).toString();
		        String entityId = args.get(1).toString();
		        
		        
		        ClientSender.notifyMyLevel( clientId, 
		                new ClientMsg( CommandCodes.ENTITY_SND_KEYUP,
		                keycode,
		                entityId ) );
		        
		        
		    }
		    
		    public int getArgCount() { return 2; }
		    
		    
		} );
		
		
		// Javascript Keydown Event
		CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_KEYDOWN, new Command()
		{
		    /*
		     * args[0] = keycode
		     * args[1] = entityId
		     */
		    
		    public void callCommand( int clientId, ArrayList<String> args )
		    {
		        String keycode = args.get(0).toString();
		        String entityId = args.get(1).toString();
		        
		        
		        ClientSender.notifyMyLevel( clientId, 
		                new ClientMsg( CommandCodes.ENTITY_SND_KEYDOWN,
		                keycode,
		                entityId ) );
		        
		        
		    }
		    
		    public int getArgCount() { return 2; }
		    
		} );
		
		CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_DRAGSTART, new Command()
		{
		    public void callCommand( int clientId, ArrayList<String> args )
		    {
		        String keycode = args.get(0).toString();
		        String entityId = args.get(1).toString();
		        
		        
		        ClientSender.notifyMyLevel( clientId, 
		                new ClientMsg( CommandCodes.ENTITY_SND_DRAGSTART,
		                keycode,
		                entityId ) );
		        
		        
		    }
		    
		    public int getArgCount() { return 0; }
		    
		} );
		
		CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_DRAGOVER, new Command()
		{
		    public void callCommand( int clientId, ArrayList<String> args )
		    {
		        String keycode = args.get(0).toString();
		        String entityId = args.get(1).toString();
		        
		        
		        ClientSender.notifyMyLevel( clientId, 
		                new ClientMsg( CommandCodes.ENTITY_SND_DRAGOVER,
		                keycode,
		                entityId ) );
		        
		        
		    }
		    
		    public int getArgCount() { return 0; }
		    
		} );
		
		// Javascript onFocus Event
		CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_FOCUS, new Command()
		{
		    public void callCommand( int clientId, ArrayList<String> args )
		    {
		        String keycode = args.get(0).toString();
		        String entityId = args.get(1).toString();
		        
		        
		        ClientSender.notifyMyLevel( clientId, 
		                new ClientMsg( CommandCodes.ENTITY_SND_FOCUS,
		                keycode,
		                entityId ) );
		        
		        
		    }
		    
		    public int getArgCount() { return 0; }
		    
		} ); 
		
		// Javascript onBlur Event
		CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_BLUR, new Command()
		{
		    public void callCommand( int clientId, ArrayList<String> args )
		    {
		        String keycode = args.get(0).toString();
		        String entityId = args.get(1).toString();
		        
		        
		        ClientSender.notifyMyLevel( clientId, 
		                new ClientMsg( CommandCodes.ENTITY_SND_BLUR,
		                keycode,
		                entityId ) );
		        
		        
		    }
		    
		    public int getArgCount() { return 0; }
		    
		} );   
		
		// Javascript onClick Event
		CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_CLICK, new Command()
		{
		    public void callCommand( int clientId, ArrayList<String> args )
		    {
		        String keycode = args.get(0).toString();
		        String entityId = args.get(1).toString();
		        
		        
		        ClientSender.notifyMyLevel( clientId, 
		                new ClientMsg( CommandCodes.ENTITY_SND_CLICK,
		                keycode,
		                entityId ) );
		        
		        
		    }
		               
		    public int getArgCount() { return 0; }
		    
		} );   
		
		
		// Javascript onDblClick event
		CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_DBLCLICK, new Command()
		{
		    public void callCommand( int clientId, ArrayList<String> args )
		    {
		        String keycode = args.get(0).toString();
		        String entityId = args.get(1).toString();
		        
		        
		        ClientSender.notifyMyLevel( clientId, 
		                new ClientMsg( CommandCodes.ENTITY_SND_DBLCLICK,
		                keycode,
		                entityId ) );
		        
		        
		    }
		                 
		    public int getArgCount() { return 0; }
		    
		} );
		
		CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_MOUSEOVER, new Command()
		{
		    public void callCommand( int clientId, ArrayList<String> args )
		    {
		        String keycode = args.get(0).toString();
		        String entityId = args.get(1).toString();
		        
		        
		        ClientSender.notifyMyLevel( clientId, 
		                new ClientMsg( CommandCodes.ENTITY_SND_MOUSEOVER,
		                keycode,
		                entityId ) );
		        
		        
		    }
		    
		    public int getArgCount() { return 0; }
		    
		} );
		
		CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_MOUSEOUT, new Command()
		{
		    public void callCommand( int clientId, ArrayList<String> args )
		    {
		        String keycode = args.get(0).toString();
		        String entityId = args.get(1).toString();
		        
		        
		        ClientSender.notifyMyLevel( clientId, 
		                new ClientMsg( CommandCodes.ENTITY_SND_MOUSEOUT,
		                keycode,
		                entityId ) );
		        
		        
		    }
		    
		    public int getArgCount() { return 0; }
		    
		} );
		
		CommandHandler.registerCommands( CommandCodes.PLAYER_RCV_MOUSEWHEEL, new Command()
		{
		    public void callCommand( int clientId, ArrayList<String> args )
		    {
		        String keycode = args.get(0).toString();
		        String entityId = args.get(1).toString();
		        
		        
		        ClientSender.notifyMyLevel( clientId, 
		                new ClientMsg( CommandCodes.ENTITY_SND_MOUSEWHEEL,
		                keycode,
		                entityId ) );
		        
		        
		    }
		    
		    public int getArgCount() { return 0; }
		    
		} );
    }
}
