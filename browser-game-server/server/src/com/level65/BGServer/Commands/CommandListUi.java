/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.level65.BGServer.Commands;

import com.level65.BGServer.Client.ClientList;
import com.level65.BGServer.Client.ClientMsg;
import com.level65.BGServer.Client.ClientSender;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class CommandListUi implements CommandList
{
    
    private HashMap<Short,Command> commandList;
    
    public HashMap<Short,Command> get( )
    {
        return this.commandList;
       
    } // ef
    
    
    public CommandListUi()
    {
    	
    }
    
}
