/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.level65.BGServer.Commands;


import java.util.ArrayList;

import com.level65.BGServer.Client.ClientMsg;
import com.level65.BGServer.Client.ClientSender;


/**
 *
 * 
 */
public class CommandListClient
{
   
    public CommandListClient()
    {
        // Message to Client
        // MTC|TargetClientId|MSG
        CommandHandler.registerCommands( CommandCodes.CLIENT_SND_MSG, new Command()
        {	
            public void callCommand( int clientId, ArrayList<String> args )
            {
                System.out.println( "Message to Client: " + args.get(0) );
                
                ClientSender.send( clientId, new ClientMsg( CommandCodes.CLIENT_SND_MSG, args.get(0) ) );
                
            }
            
            public int getArgCount() { return 2; }
            
        });

        CommandHandler.registerCommands( CommandCodes.CLIENT_SND_CLOSE, new Command()
        {	
            public void callCommand( int clientId, ArrayList<String> args )
            {
                System.out.println( "closing" );
                ClientSender.send( clientId, 
                        new ClientMsg( CommandCodes.CLIENT_RCV_CLOSE ) );
                
            }
            
            public int getArgCount() { return 2; }
            
        });
    }
    
    
    
    
}
