/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.BGServer.Client;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class ClientList 
{
    
    private static int nextClientId = -1;
    public static ArrayList<ClientInfo> masterList = new ArrayList<ClientInfo>();
    //private static Queue<String> = new Queue<String>();
    
    private ClientList() { }
    
    
    /**
     * Registers a client's connection with the server, making it available to
     * other clients on the server.
     * @param client 
     */
    public synchronized static void register( Thread inputThread, Socket client )
    {
    	++nextClientId;
        masterList.add( nextClientId, new ClientInfo( inputThread, client, nextClientId ) );
        
    }
       
    
    /**
     * Unregisters a client's connection to the server.  This method MUST be
     * synchronized to prevent multiple threads from simultaneously removing
     * clients (data corruption will occur).
     * @param clientId 
     */
    public synchronized static void unregister( int clientId )
    {
        System.out.println( "Unregister" );
        if ( hasClient( clientId ) ) masterList.remove( clientId );
        
    }
    
    
    /**
     * Sets the reference to the client's Output Thread.  We must have a
     * reference to the thread so that it can be shutdown when the clientThis cannot be set 
     * when the connection is registered, because the outgoing thread is not
     * started until the client handshake is complete.
     * @param clientId
     * @param outputThread 
     */
    public static void setOutputThread( int clientId, Thread outputThread )
    {
        masterList.get( clientId ).setOutputThread( outputThread );
        
    }
    
    /**
     * Returns the time that the client connected to the server. Useful for
     * figuring out how long the client has been connected.
     * @param clientId
     * @return 
     */
    public static String getConnectTimestamp( int clientId )
    {
        return masterList.get( clientId ).ConnectTimestamp.toString();
        
    }
    
    
    /**
     * Returns a thread reference to the thread running this particular client.
     * Useful for getting a reference to use with thread methods like wait
     * and notify.
     * @param clientId
     * @return 
     */
    public static Thread getInputThread( int clientId )
    {
        return masterList.get( clientId ).inputThread;
        
    }
    
    
    /**
     * Returns a handle to this client's output thread, if one exists.  This is
     * necessary for clean shutdowns/disconnects so that the ClientList can
     * close a client's threads when it disconnects.
     * @param clientId
     * @return 
     */
    public static Thread getOutputThread( int clientId )
    {
        return masterList.get( clientId ).outputThread;
        
    }
    
    
    
    /**
     * Returns the IP address of the client as a string.  
     * @param clientId
     * @return 
     */
    public static String getIp( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return masterList.get( clientId ).client.getInetAddress().toString();
        
        }
        else
        {
            return "";
            
        }
    }
    
    
    
    /**
     * Pushes a msg onto the client's outgoing buffer.  This method MUST NOT be
     * made synchronized.  Each buffer is thread-safe, so that threads only
     * have to wait if they are trying to write to the same buffer.  Multiple
     * threads can safely write to DIFFERENT buffers simultaneously, as long as
     * this method remains NOT synchronized.
     * @param clientId
     * @param msg 
     */
    public static void bufferPush( int clientId, String msg )
    {
        if ( hasClient( clientId ) ) 
        {
            masterList.get( clientId ).bufferPush( msg );
   
        }
    }
    
    
    /**
     * Returns the first item in the buffer, or null if the buffer is currently
     * empty. Each buffer is thread-safe, so that threads only
     * have to wait if they are trying to write to the same buffer.  Multiple
     * threads can safely write to DIFFERENT buffers simultaneously, as long as
     * this method remains NOT synchronized.
     * @param clientId
     * @return 
     */
    public static String bufferPop( int clientId )
    {
        String item = null;
        if ( !hasClient( clientId ) ) return "";
        
        try 
        {
            return masterList.get( clientId ).bufferPop();
        
        }
        catch ( InterruptedException e )
        {
            
        }
        
        return item;
    }
    
    
    /**
     * Returns the connection port on the client side.
     * @param clientId
     * @return 
     */
    public static int getPort( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return masterList.get( clientId ).client.getPort();
        
        }
        else
        {
            return 0;
            
        }
    }
    
    
    /**
     * Returns the client's current username.
     * @param clientId
     * @return 
     */
    public static String getUsername( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return masterList.get( clientId ).username;
        
        }
        else
        {
            return "";
            
        }
    }
    
    
    /**
     * Set's the username for a specific client.
     * @param clientId
     * @param _username 
     */
    public synchronized static void setUsername( int clientId, String _username )
    {
        if ( hasClient( clientId ) )
        {
            masterList.get( clientId ).username = _username;
        
        }   
    }
    
    public static ClientInfo getInfo( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return masterList.get( clientId );
            
        }
        else
        {
            return null;
            
        }
        
    }
    /**
     * Returns the master client list as an ArrayList.  
     * @return 
     */
    public static ArrayList<ClientInfo> getList()
    {
        return masterList;
        
    }
    
    
    /**
     * Returns the current size of the master client list. Useful for knowing
     * how many clients are currently registered with the server.
     * @return 
     */
    public static int getListSize()
    {
        return masterList.size();
        
    }
    
    
    /**
     * Gets the current queue from the client info object.
     * @param clientId
     * @return 
     */
    public static Queue<String> getQueue( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return masterList.get( clientId ).bufferOut;
        
        }
        else
        {
            // TODO: Check if null is acceptable as a return.
            return null;
            
        }
    }
    
    
    public static void setChunkId( int clientId, int chunkId )
    {
        if ( hasClient( clientId ) )
        {
            masterList.get( clientId ).setChunkId( chunkId );
            
        }
    }
    
    public static int getChunkId( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return masterList.get( clientId ).playerChunkId;
            
        }
        else
        {
            return -1;
            
        }
        
    }
    
    
    public static void setLevelId( int clientId, int levelId )
    {
        if ( hasClient( clientId ) )
        {
            masterList.get( clientId ).setLevelId( levelId );
            
        }
    }
    
    public static int getLevelId( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return masterList.get( clientId ).playerLevelId;
            
        }
        else
        {
            return -1;
            
        }
        
    }
    
    
    public static void setEntityId( int clientId, int entityId )
    {
        if ( hasClient( clientId ) )
        {
            masterList.get( clientId ).setEntityId( entityId );
              
        }
    }
    
    
    public static int getEntityId( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return masterList.get( clientId ).playerEntityId;
            
        }
        else
        {
            return -1;
            
        }
    }
    
    
    
    /**
     * Returns the clientId that will be assigned to the next client that
     * connects to the server and registers a connection.
     * @return 
     */
    public static int getNextClientId()
    {
       return nextClientId + 1;
       
    }
    
    
    /**
     * Returns whether the ClientList has a client with the specified clientId.
     * Throws an InvalidClientIdException when a clientId does not exist.
     * @param clientId
     * @return 
     */
    public static boolean hasClient( int clientId )
    {
        try
        {
            if ( masterList.get( clientId ) != null )
            {
                return true;

            }
            else
            {
                throw new InvalidClientIdException( "Client ID not found." );
                
            }
            
        }
        catch ( InvalidClientIdException e )
        {
            e.printStackTrace();
            
        }
        
        return false;
         
    }
}
