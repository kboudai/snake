/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.level65.BGServer.Client.Protocol;

import java.io.UnsupportedEncodingException;

/**
 *
 * @author Home
 */
public class WebsocketDataFrame13 implements WebsocketDataFrame
{

    private byte opcode = 0x0;
    private boolean isControlFrame = false;
    private boolean isCloseFrame = false;
    private boolean isPing = false;
    private boolean isPong = false;
    private String frameType = "";
    private String text = "";
    
    public WebsocketDataFrame13( byte[] headerBytes, byte[] payload )
    {
        
        try 
        {
            this.opcode = (byte)( headerBytes[0] & (byte) 127 );
            
            
            
            this.text = new String( payload, "UTF-8" );
        
        }
        catch ( UnsupportedEncodingException e )
        {
            
        }
        
    }
    
    public boolean isText()
    {
        return ( this.opcode == 0x1 );
        
    }
    
    
    @Override
    public boolean isControlFrame() 
    {
        return false;
        
    }

    public String frameType()
    {
        return this.frameType;
        
    }
    
    public boolean isCloseFrame()
    {
        return ( this.opcode == 0x8 );
        
    }
    
    @Override
    public boolean isPing() 
    {
        return ( this.opcode == 0x9 );
    }

    @Override
    public boolean isPong() 
    {
        return ( this.opcode == 0xA );
        
    }

    @Override
    public String text() 
    {
        return this.text;
    }

    
    
}
