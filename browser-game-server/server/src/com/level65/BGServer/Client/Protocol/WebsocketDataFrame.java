/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.level65.BGServer.Client.Protocol;

/**
 *
 * @author Home
 */
public interface WebsocketDataFrame 
{
    
    abstract boolean isControlFrame();
    abstract boolean isPing();
    abstract boolean isCloseFrame();
    abstract boolean isPong();
    abstract boolean isText();
    abstract String text();
    
}
