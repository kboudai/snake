/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.BGServer.Client;

import com.level65.BGServer.Game.GameHandler;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class ClientSender 
{
    /**
     * Send a message to a specific client.
     * @param clientId
     * @param msg 
     */
    public static void send( int clientId, ClientMsg clientMsg )
    {
        ClientList.bufferPush( clientId, clientMsg.msg );
        
        
    } // ef
    
//    public static void sendAll( ClientMsg clientMsg )
//    {
//        ClientList.bufferPushAll( clientMsg.msg );
//        
//        
//    }
    
    
    
    /**
     * Sends a message to all clients in a level, except for the source client.
     * @param sourceClientId
     * @param clientMsg 
     */
    public static void notifyMyLevel( int sourceClientId, ClientMsg clientMsg )
    {
        int levelId = ClientList.getLevelId( sourceClientId );
        for ( int clientId : GameHandler.getLevel( levelId ).getClients() )
        {
            if ( clientId != sourceClientId ) send( clientId, clientMsg );
        }
        
        
    }
    
    public static void notifyMyLevel( int levelId, int sourceClientId, ClientMsg clientMsg )
    {
        for ( int clientId : GameHandler.getLevel( levelId ).getClients() )
        {
            if ( clientId != sourceClientId ) send( clientId, clientMsg );
        }
        
        
    }
    
    /**
     * Sends a message to all clients in a chunk, including the source client.
     * @param chunkId
     * @param clientMsg 
     */
    public static void notifyChunk( int levelId, int chunkId, ClientMsg clientMsg )
    {
        // TODO: Add chunk player messaging.
        
    }
    
    
    /**
     * Sends a message to all clients in a chunk, except for the source client.
     * @param sourceClientId
     * @param clientMsg 
     */
    public static void notifyMyChunk( int sourceClientId, ClientMsg clientMsg )
    {
        // TODO: Add messaging all players except the source.
        int chunkId = ClientList.getChunkId( sourceClientId );
        
        
        
    }
    
    
    /**
     * Sends a message to all currently connected clients.
     * @param sourceClientId
     * @param clientMsg 
     */
    public static void notifyAll( ClientMsg clientMsg )
    {
        for ( ClientInfo info : ClientList.getList() )
        {
//            send( info.hashCode(), clientMsg );
        	send( info.clientId, clientMsg );
            
        }
    }
    
//    public static void notifyAllS( ClientMsg clientMsg )
//    {
//    	sendAll(clientMsg);
//    }
   
    
    /**
     * Sends a message to all clients in a level.
     * @param levelId
     * @param clientMsg 
     */
    public static void notifyLevel( int levelId, ClientMsg clientMsg )
    {
        
        
    }
    
    
    
   
}
