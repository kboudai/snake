/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.level65.BGServer.Client;

import com.level65.BGServer.Client.Protocol.WebsocketProtocol;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class ClientOutputHandler implements Runnable
{
    
    private int clientId;
    private WebsocketProtocol protocol;
    DataOutputStream toClient;
//    public static CopyOnWriteArrayList<String> notifyAllBuffer = new CopyOnWriteArrayList<String>();
//    public int nABPos = 0;
    
    public ClientOutputHandler( int _clientId, DataOutputStream _toClient, 
            WebsocketProtocol _protocol ) throws IOException
    {
        this.toClient = _toClient;
        this.clientId = _clientId;
        this.protocol = _protocol;
        
        
    }
    
    public void run()
    {
        // TODO: Handling for 
        for ( ;; )
        {
            try
            {
               synchronized ( ClientList.getQueue( clientId ) )
               {
//                   System.out.println( "Client Queue" );
                   while ( ClientList.getQueue( clientId ).isEmpty() )
                   {
                	   ClientList.getQueue( clientId ).wait();
                   }
                   String msg = ClientList.bufferPop( this.clientId );
                   
                   System.out.println( "Sent message: " + msg + " buffer processed" );
                   this.protocol.sendMessage( this.toClient, msg );
                   
               }
               
//               synchronized ( notifyAllBuffer )
//               {
//            	   String msg;
//                   while ( notifyAllBuffer.size() == nABPos )
//                   {
//                	   notifyAllBuffer.wait();
//                	   System.out.println( "All Client Queues" );
//                   }
//                   
//                   msg = notifyAllBuffer.get(nABPos++);
//                   System.out.println( "Sent message: " + msg + " buffer processed" );
//                   this.protocol.sendMessage( this.toClient, msg );
//                   
//               }
                
            }
            catch ( InterruptedException e )
            {
                break;
                
            }
        }
    }
    
    
}
