/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.level65.BGServer.Client;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class ClientMsg 
{
    public String msg = "";
    private String delimiter = "\037";
    
    
    public ClientMsg( short cmdId, int arg1 )
    {
        msg = Short.toString( cmdId );
        msg += delimiter;
        msg += Integer.toString( arg1 );    
        
        
    }
    
   
    
    public ClientMsg( short cmdId, int arg1, double arg2, double arg3 )
    {
        msg = Short.toString( cmdId );
        msg += delimiter;
        msg += Integer.toString( arg1 );
        msg += delimiter;
        msg += Double.toString( arg2 );
        msg += delimiter;
        msg += Double.toString( arg3 );
        
        
    }
    
    
    
    public ClientMsg( short cmdId, int arg1, int arg2 )
    {
        msg = Short.toString( cmdId );
        msg += delimiter;
        msg += Integer.toString( arg1 );
        msg += delimiter;
        msg += Integer.toString( arg2 );
        
    }
    
    
    
    public ClientMsg( short cmdId, double arg1, double arg2 )
    {
        msg = Short.toString( cmdId );
        msg += delimiter;
        msg += Double.toString( arg1 );
        msg += delimiter;
        msg += Double.toString( arg2 );
        
    }
    
    
    
    public ClientMsg( short cmdId, String ... args )
    {
        msg = Short.toString( cmdId );
        
        for ( String arg : args )
        {
            msg += delimiter;
            msg += arg;
            
        } 
    }
    
    
}
