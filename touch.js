var Touch = 
{
	ongoingTouches : [],
	touches : null,
	deltaX : null,
	deltaY : null,
	isFullscreen : false,
	isJoined : false,
	isReady : false,

	init : function()
	{
		canvas.addEventListener("touchstart", this.handleStart, false);
		canvas.addEventListener("touchend", this.handleEnd, false);
		canvas.addEventListener("touchcancel", this.handleCancel, false);
		canvas.addEventListener("touchleave", this.handleEnd, false);
		canvas.addEventListener("touchmove", this.handleMove, false);
	},

	handleStart : function(evt)
	{
		evt.preventDefault();
		Touch.touches = evt.changedTouches;

		if (!Touch.isJoined)
		{
			alert('sent join');
			DefaultCmds.sndJoinWorld();
			Touch.isJoined = true;
		}
		else if (!Touch.isReady)
		{
			alert('sent ready');
	    	DefaultCmds.sndReady();
			Touch.isReady = true;
		}

		// if (!Touch.isFullscreen)
		// {
		// 	alert('hello');
		// 	if (canvas.requestFullscreen)
		// 	{
		// 	  canvas.requestFullscreen();
		// 	}
		// 	else if (canvas.mozRequestFullScreen)
		// 	{
		// 	  canvas.mozRequestFullScreen();
		// 	}
		// 	else if (canvas.webkitRequestFullscreen)
		// 	{
		// 	  canvas.webkitRequestFullscreen();
		// 	}
		// 	Touch.isFullscreen = true;
		// }

		for (var i = 0; i < Touch.touches.length; i++)
		{
			Touch.ongoingTouches.push(Touch.touches[i]);
		}
	},

	handleMove : function(evt)
	{
		evt.preventDefault();
	},

	handleEnd : function(evt)
	{
		evt.preventDefault();
		Touch.touches = evt.changedTouches;

		for (var i = 0; i < Touch.touches.length; i++)
		{
			var idx = Touch.ongoingTouchIndexById(Touch.touches[i].identifier);

			Touch.deltaX = Touch.ongoingTouches[idx].pageX - Touch.touches[i].pageX;
			Touch.deltaY = Touch.ongoingTouches[idx].pageY - Touch.touches[i].pageY;

			// alert('deltaX ' + Touch.deltaX + ' ' + ' deltaY ' + Touch.deltaY);

			if (Math.abs(Touch.deltaX) > Math.abs(Touch.deltaY))
			{
				if (Touch.deltaX < 0 &&
				(snakes[0].direction != "left" && snakes[0].direction != "right"))
				{
					// alert('right');
			    	keyBuffer[0].push("right");
				}
				else if ( snakes[0].direction != "right" && 
							snakes[0].direction != "left")
				{
					// alert('left');
			    	keyBuffer[0].push("left");
				}
			}
			else
			{
				if (Touch.deltaY < 0 && (snakes[0].direction != "up" &&
											snakes[0].direction != "down"))
				{
					// alert('down');
			    	keyBuffer[0].push("down");
				}
				else if ( snakes[0].direction != "down" &&
							snakes[0].direction != "up")
				{
					// alert('up');
			    	keyBuffer[0].push("up");
				}
			}
			Touch.ongoingTouches.splice(i, 1);  // remove it; we're done
		}
	},

	handleCancel : function(evt)
	{
		evt.preventDefault();
		Touch.touches = evt.changedTouches;

		for (var i = 0; i < Touch.touches.length; i++)
		{
			Touch.ongoingTouches.splice(i, 1);  // remove it; we're done
		}
	},

	ongoingTouchIndexById : function(idToFind)
	{
		for (var i = 0; i < Touch.ongoingTouches.length; i++)
		{
			var id = Touch.ongoingTouches[i].identifier;

			if (id == idToFind)
			{
				return i;
			}
		}
		return -1;    // not found
	}
}




