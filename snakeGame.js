// Canvas vars
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');

// Cell drawing vars
var size = 10;
context.lineWidth = 2;

// Game vars
var snakes = [];
var food = null;
// var food = new food();
var GAMEID;
var posID;

// score vars
var scores = [];
var scoreText;

// Iterator
var i;

// Keyboard buffer to fix collision bug
var keyBuffer = [];
keyBuffer[0] = [];
keyBuffer[1] = [];

function init()
{
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) )
	{
		// alert('YAY PHONE');
		Touch.init();
	}
	else
	{
		console.log('nobody likes you computer');
	}
	
	// Open socket and connect to server
	if ( !bgClient.connect() )
	{
		alert("Connection failed: WebSocket not " +
					"supported by browser");
		// Try and reconnect 10 times then kill
		// Make var in main that kills game

	}

	// snakes.push(new snake(0, 5, 0, 'white'));
	// snakes[snakes.length-1].init(true);
	// snakes.push(new snake(1, 5, 1, color2));
	// snakes[snakes.length-1].init(true);
	draw();
	// start();
};

function update ()
{
	// Update snakes
	for(var s in snakes)
	{
		if (keyBuffer[s].length > 0)
		{
			snakes[s].direction = keyBuffer[s].pop();
			if(snakes[s].isPlayer)
			{
				switch(snakes[s].direction)
				{
					case "left":
						DefaultCmds.sndPlayerKeypress
							(snakes[0].entityID, KEY.LEFT_ARROW);
						break;

					case "right":
						DefaultCmds.sndPlayerKeypress
							(snakes[0].entityID, KEY.RIGHT_ARROW);
						break;

					case "up":
		            	DefaultCmds.sndPlayerKeypress
		            		(snakes[0].entityID, KEY.UP_ARROW);
						break;

					case "down":
		            	DefaultCmds.sndPlayerKeypress
		            		(snakes[0].entityID, KEY.DOWN_ARROW);
						break;
	            		
				}
			}
		}
		if(snakes[s].update())
		{
			DefaultCmds.sndGameOver( snakes[s].entityID );
		}
	}

	// Food collision checking
	if(food != null) food.update();

	draw();
};

function draw()
{
	// Clear canvas with black
	context.fillRect(0, 0, canvas.width, canvas.height);

	// Paint snakes
	for(var s in snakes)
	{
		snakes[s].draw();
	}
	
	// Paint food
	if(food != null) food.draw();


	// if (window.mozRequestAnimationFrame)
	// 	GAMEID = window.mozRequestAnimationFrame(update);
	// else if (window.requestAnimationFrame)
	// 	GAMEID = window.requestAnimationFrame(update);
	// else if (window.webkitRequestAnimationFrame)
	// 	GAMEID = window.webkitRequestAnimationFrame(update);
	// else
	// 	console.log('FAIL ANIM');
	
};

// Start game
function start()
{
	if (GAMEID == undefined) 
	{
		console.log('Start');
		GAMEID = setInterval(update,50);
		posID = setInterval(posUpdate, 1000);
		// request anim test
		// var requestAnimationFrame = window.requestAnimationFrame || 
		// 							window.mozRequestAnimationFrame ||
		// 							window.webkitRequestAnimationFrame || 
		// 							window.msRequestAnimationFrame;
	}
	else
	{
		console.log('Stop');
		stop();
	}
};

// Stop/pause game
function stop()
{
	clearInterval(GAMEID);
	clearInterval(posID);
	posID = GAMEID = undefined;
	// request anim test
	// var cancelAnimationFrame =	window.cancelAnimationFrame || 
	// 							window.mozCancelAnimationFrame;
};

function GameOver(entityID)
{
	stop();
	console.log('Player ' + entityID + ' Lost!');
	DefaultCmds.sndCloseClient();


	// scores.push( (snakes[0].body.length)*10 );

	// var count = 1;
	// for (i = scores.length - 1; i >= 0; i--)
	// {	
	// 	if (i == scores.length - 1)
	// 	{
	// 		scoreText = "New score: " + scores[i] + "\n\n" +
	// 				 "Previous scores:\n";

	// 	}
	// 	else
	// 	{
	// 		scoreText += count++ + ". " + scores[i] + "\n"
	// 	};
	// };

	// alert(scoreText);

	// snakes = [];

	// init();
};

function posUpdate()
{
	DefaultCmds.sndPlayerPos(snakes[0].entityID, 
			snakes[0].body[0].x, snakes[0].body[0].y);
}

document.addEventListener
('keydown', function(event) 
{
	// console.log(event.keyCode);
	// <-----------------------Player One---------------------------------->

	if(event.keyCode == 37 && 
		(snakes[0].direction != "right" && snakes[0].direction != "left")) 
	{
    	//console.log('Left was pressed');
    	keyBuffer[0].push("left");
	}

	if(event.keyCode == 39 && 
		(snakes[0].direction != "left" && snakes[0].direction != "right")) 
	{
    	//console.log('Right was pressed');
    	keyBuffer[0].push("right");
	}

	if(event.keyCode == 38 && 
		(snakes[0].direction != "down" && snakes[0].direction != "up")) 
	{
    	//console.log('Up was pressed');
    	keyBuffer[0].push("up");
	}

	if(event.keyCode == 40 && 
		(snakes[0].direction != "up" && snakes[0].direction != "down"))  
	{
    	//console.log('Down was pressed');
    	keyBuffer[0].push("down");
	}

	// <--------------------------Player Two------------------------------->

	// if(event.keyCode == 65 && snakes[1].direction != "right") 
	// {
 //    	//console.log('A was pressed');
 //    	keyBuffer[1].push("left");
	// }
	
	// if(event.keyCode == 68 && snakes[1].direction != "left") 
	// {
 //    	//console.log('D was pressed');
 //    	keyBuffer[1].push("right");
	// }
	
	// if(event.keyCode == 87 && snakes[1].direction != "down") 
	// {
 //    	//console.log('W was pressed');
 //    	keyBuffer[1].push("up");
	// }
	
	// if(event.keyCode == 83 && snakes[1].direction != "up") 
	// {
 //    	//console.log('S was pressed');
 //    	keyBuffer[0].push("down");
	// }

	// <---------------------End of snake controls------------------------->

	// <---------------------Snake game controls--------------------------->

	if(event.keyCode == 32) 
	{
    	//console.log('Space was pressed');
    	// start();
    	DefaultCmds.sndReady();
	}

	if(event.keyCode == 74) 
	{
		//console.log('J was pressed');
		DefaultCmds.sndJoinWorld();
	}

	if(event.keyCode == 80) 
	{
		//console.log('P was pressed');
		start();
	}


});


